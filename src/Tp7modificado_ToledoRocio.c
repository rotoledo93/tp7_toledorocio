/*
===============================================================================
 Name        : Tp7modificado_ToledoRocio.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/
# define AddrPINSEL0 0x4002C000
# define AddrPINSEL4 0X4002C010
# define AddrFIO0DIR 0x2009C000
# define AddrFIO0SET 0x2009C018
# define AddrFIO0CLR 0x2009C01C
# define AddrFIO0PIN 0x2009C014
# define AddrFIO2PIN 0x2009C054
# define AddrFIO2SET 0x2009C058
# define AddrFIO2CLR 0x2009C05C
# define AddrFIO2DIR 0x2009C040
# define AddrEXTINT  0x400FC140
# define AddrEXTMODE 0x400FC148
# define AddrEXTPOLAR 0x400FC14C
# define AddrISER0 0xE000E100

unsigned int volatile *const	PINSEL0=(unsigned int*)	AddrPINSEL0;
unsigned int volatile *const	PINSEL4=(unsigned int*)	AddrPINSEL4;
unsigned int volatile *const	FIO2DIR=(unsigned int*)	AddrFIO2DIR;
unsigned int volatile *const	FIO2SET=(unsigned int*)	AddrFIO2SET;
unsigned int volatile *const	FIO2CLR=(unsigned int*)	AddrFIO2CLR;
unsigned int volatile *const	FIO2PIN=(unsigned int*)	AddrFIO2PIN;
unsigned int volatile *const	ISER0=(unsigned int*)	AddrISER0;
unsigned int volatile *const	EXTINT=(unsigned int*)	AddrEXTINT;
unsigned int volatile *const	EXTMODE=(unsigned int*)	AddrEXTMODE;
unsigned int volatile *const	EXTPOLAR=(unsigned int*)	AddrEXTPOLAR;
unsigned int volatile *const FIO0DIR = (unsigned int*) AddrFIO0DIR;
unsigned int volatile *const FIO0SET = (unsigned int*) AddrFIO0SET;
unsigned int volatile *const FIO0CLR = (unsigned int*) AddrFIO0CLR;
unsigned int volatile *const FIO0PIN = (unsigned int*) AddrFIO0PIN;
void retardo(unsigned int);
void retardo2(unsigned int);

unsigned int time=500000;
unsigned int time2=4000;
const int display[10]={0x3F,0x06,0x5B,0x4F,0x66,0x6D,0x7D,0x07,0x7F,0x67};//CARGAMOS EL ARREGLO CON LOS VALORES PARA ENCENDER EL DISPLAY

int main(void) {
	*PINSEL4|=(1<<20);//PIN P2.10 COMO INTERRUPCION EXTERNA INT0
	*PINSEL4&=~(1<<21);
	*PINSEL4|=(1<<22);//PIN P2.11 COMO INTERRUPCION EXTERNA INT1
	*PINSEL4&=~(1<<23);

	*FIO2DIR|=(1<<0);//DECLARAMOS LOS PINES DE SALIDA, SON LOS SEGMENTOS DEL DISPLAY
	*FIO2DIR|=(1<<1);//P2.0;P2.1..
	*FIO2DIR|=(1<<2);
	*FIO2DIR|=(1<<3);
	*FIO2DIR|=(1<<4);
	*FIO2DIR|=(1<<5);
	*FIO2DIR|=(1<<6);
	*FIO0DIR|=(1<<9);//LED DE SALIDA p0.9
	*FIO2CLR|=(1<<0);//INICIALIZO EN CERO AL DISPLAY Y AL LED
	*FIO2CLR|=(1<<1);
	*FIO2CLR|=(1<<2);
	*FIO2CLR|=(1<<3);
	*FIO2CLR|=(1<<4);
	*FIO2CLR|=(1<<5);
	*FIO2CLR|=(1<<6);
	*FIO0CLR|=(1<<9);



*EXTMODE|=(1<<0);//INTERRUPCIO EINT0 POR FLANCO
*EXTPOLAR|=(1<<0); //DE SUBIDA EL FLANCO DE EINT0
*EXTMODE|=(1<<1);//INTERRUPCIO EINT1 POR FLANCO
*EXTPOLAR&=~(1<<1); //DE BAJADA EL FLANCO DE EINT1
*EXTINT|=(1<<1);//LIMPIO BANDERA
*EXTINT|=(1<<0);//LIMPIO BANDERA

*ISER0|=(1<<18);//HABILITACION DE EINT0
*ISER0|=(1<<19);//HABILITACION DE EINT1



    while(1) {

    	*FIO0SET|=(1<<9);//PRENDIDO Y APAGADO DE LED
    	retardo2(time2);
    	*FIO0CLR|=(1<<9);
    	retardo2(time2);


    }
    return 0 ;
}

void EINT0_IRQHandler(void)
{
	static int contador1=0;//USAMOS PARA CONTAR CUANTAS VECES SE ENTRA A LA INTERRUPCION
	static int prueba1=1;//DECLARO ESTA VARIABLE QUE SE INICIALIZA UNA SOLA VEZ PARA PODER HACER DISTINTAS COSAS
	                     //CADA VEZ QUE ENTRA A LA INTERRUPCION

		if(prueba1==1)//PRIMERA VEZ QUE PRESIONAMOS EL BOTON, OSEA ENTRA A LA INTERRUPCION
	{
		time2=200000;//ASIGNAMOS ESTE TIEMPO PARA EL PARPADEO DE LED

	}
	else// SINO QUIERE DECIR QUE ES LA SEGUNDA VEZ Q APRETAMOS EL BOTON OSEA TIENE QUE CAMBIAR LA FRECUENCIA
		//DEL LED POR LO TANTO ASIGNAMOS OTRO VALOR A TIME2
	{

		time2=20000;
		prueba1=0;//LO HACEMOS CERO PARA QUE EN LA PROXIMA VEZ QUE APRETEMOS EL PULSADOR
		//CON EL INCREMENTO DEL FINAL SEA 1 NUEVAMENTE Y VOLVEMOS A REPETIR


	}


contador1++;

	*EXTINT|=(1<<0);//LIMPIO BANDERA
	prueba1++;//INCREMENTAMOS EL VALOR DE PRUEBA

}
void EINT1_IRQHandler(void)
{
static int contador2=0;
static int prueba=0;//IDEM A LA INTERRUPCION ANTERIOR SOLO QUE ACA PROCEDEMOS AL PRENDIDO DEL DISPLAY

if(prueba<10)
	{
			*FIO2PIN = (display[prueba]);
			retardo(time);
	}
if(prueba==9)// QUIERE DECIR QUE LLEGAMOS AL 9 POR LO QUE DEBEMOS CAMBIAR POR NIVEL
{
	*EXTMODE&=~(1<<1);//INTERRUPCIO EONT1 POR NIVEL
	*EXTPOLAR&=~(1<<1);// POR BAJO
prueba=0;// LO PONEMOS A CERO Y DECREMENTAMOS PARA QUE CON EL INCREMENTO DEL FINAL VOLVAMOS A EMPEZAR
prueba--;
}


contador2++;

*EXTINT|=(1<<1);//LIMPIA BANDERA
prueba++;
	}
void retardo(unsigned int time)
{
	unsigned int i;

	for(i=0; i<time; i++){}
}
void retardo2(unsigned int time2)
{
	unsigned int i;

	for(i=0; i<time2; i++){}
}

